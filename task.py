#!/usr/bin/env python3

class Task(object):
    """Base-Class to implement a simple task"""

    def __init__(self, dispatcher):
        self._dispatcher = dispatcher
        self._watchlist = []

    def process(self, message):
        """Receive a message from the dispatcher. Check if the id is in our watchlist.
        If so pass it to out internal processing."""
        if message["id"] in self._watchlist:
            self._processWatched(message)

    def _processWatched(self, message):
        """Stub. Implement your own :) """
        pass


    def _addWatchlistID(self, ident):
        """Adds an id to our watchlist"""
        self._watchlist.append(ident)

