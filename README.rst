sOss Light Control for Home Assistant
=====================================


You can use this component to control a sOss-enabled light with your Home Assistant instance.

Currently only hard-coded configurations are supported :o

You may want to place this git in:
```
<hass_config>/custom_components/light/
```
and want to add something to your configuration like this:
```
light:
  - platform: light
```
