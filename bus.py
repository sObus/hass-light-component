#!/usr/bin/env python3

class Bus(object):
    """Base-Class to derive your own fancy sOss-Busses"""

    def __init__(self, dispatcher):
        self._dispatcher = dispatcher
        dispatcher.registerBus(self)


    def send(self, message):
        """Stub. Implement your own send-method according to your bus."""


