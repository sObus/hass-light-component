#!/usr/bin/env python3

from homeassistant.components.light import Light
from homeassistant.components.light import (
    ATTR_BRIGHTNESS, ATTR_RGB_COLOR, ATTR_TRANSITION,
    SUPPORT_BRIGHTNESS, SUPPORT_RGB_COLOR, SUPPORT_TRANSITION, PLATFORM_SCHEMA)


import socket
import urllib.request, urllib.parse, urllib.error
import urllib.parse
import _thread
import fcntl
import struct
import time
import logging
from .udpbus import UdpBus
from .dispatcher import Dispatcher

_LOGGER = logging.getLogger(__name__)

DOMAIN = "light"
REQUIREMENTS = []

SUPPORT_RGB_LED = (SUPPORT_BRIGHTNESS | SUPPORT_RGB_COLOR )

class SOss(object):
    def __init__(self, bindIP, hass, config, add_devices):
        self._dispatcher = Dispatcher()
        self._udp = UdpBus(self._dispatcher, bindIP)
        self._hass = hass
        self._config = config
        self._add_devices = add_devices

    def receiveFromBus(self, message):
        pass

    def sendToBus(self, message):
        self._udp.send(message)

sOss = None

def setup_platform(hass, config, add_devices, discovery_info=None):
    # start sOss
    bind = config["bindIP"]
    sOss = SOss(bind, hass, config, add_devices)
    _LOGGER.info("Setting up sOss UDP-Bus on host {}".format(bind))

    for v in config["lights"]:
        _LOGGER.info("Setting up sOss-LED for name: {}".format(v["name"]))
        add_devices([RgbLight(v["name"], sOss)])


class RgbLight(Light):
    def __init__(self, serviceId, sOss):
        self._sOss = sOss
        self._name = serviceId
        self._color = [255, 255, 255]
        self._brightness = 255
        self._isOn = True

    @property
    def name(self):
        return self._name

    @property
    def brightness(self):
        return self._brightness

    @property
    def rgb_color(self):
        """Return the color property."""
        return self._color

    @property
    def is_on(self, **kwargs):
        return self._isOn

    def turn_on(self, **kwargs):
        if ATTR_BRIGHTNESS in kwargs:
            self._brightness = kwargs[ATTR_BRIGHTNESS]
            print("setting brightness to {}".format(self._brightness))
        if ATTR_RGB_COLOR in kwargs:
            self._color = kwargs[ATTR_RGB_COLOR]
            print("setting color to {}".format(self._color))
        self.upate()
        self._isOn = True
        self._send()
        print("Turning on..")

    def turn_off(self, **kwargs):
        self._isOn = False
        print("Turning off")
        self._send()

    def _send(self):
        if self._isOn == False:
            c = [0]*4
        else:
            c = [x for x in self._color] + [sum(self._color)/3.0]
            c = [int(x*256*(self._brightness/255)) for x in c]
        self._sOss.sendToBus(
            {
                "method": "request",
                "id": self._name,
                "value": ",".join([str(x) for x in c]),
                "reqId": int(time.time())
            })

    def upate(self):
        pass

    @property
    def supported_features(self):
        """Flag supported features."""
        return SUPPORT_RGB_LED
