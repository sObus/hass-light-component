#!/usr/bin/env python3

from . import bus
import socket
import urllib.request, urllib.parse, urllib.error
import urllib.parse
import _thread
import fcntl
import struct

# determine the primary IPv4 for a given inerface.
# found on http://stackoverflow.com/a/24196955
# thank you Martin Konecny
def get_ip_address(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', ifname[:15])
    )[20:24])

# well, as long as we are running on linux this should work.
# found on http://stackoverflow.com/a/7221783
# thank you hamstergene
SO_BINDTODEVICE = 25

class UdpBus(bus.Bus):
    """Implements a sOss-Bus over UDP.
    Since this implementation suppresses all traffic that is send from the interface it runs on (e.g. from other instances of this code) we won't be able to interoperate with multiple sOss-Bus instances on the same interface and port."""

    def __init__(self, dispatcher, bindAddress, port=31338):
        """Initializes a new UdpBus.
        dispatcher: Dispatcher-object that receives all incoming messages
        bindInterface: Interface we will bind our broadcast to
        port: Port to bind to."""
        super(UdpBus, self).__init__(dispatcher)

        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self._socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self._socket.bind((bindAddress,port))

        self._run = True
        self._port = port

        self._rx = _thread.start_new_thread(self._rxThread, ())

    def _rxThread(self):
        while self._run:
            (raw, addr) = self._socket.recvfrom(1024)

            message = dict([(k, v[0]) for k,v in list(urllib.parse.parse_qs(raw).items())])
            # give it to the dispatcher
            self._dispatcher.receiveFromBus(self, message)

    def send(self, message):
        """Will send a sOss-Message over UDP.
        message: Dict containing the fields to be send"""
        #convert to querystring
        query = urllib.parse.urlencode(message)
        query = query.replace("%2C", ",").encode("UTF-8")

        self._socket.sendto(query, ('<broadcast>', self._port))

