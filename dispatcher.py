#!/usr/bin/env python3

import time
import queue
import _thread
from . import task
import sys

class Dispatcher:
    def __init__(self):
        # Initialise list of Tasks (object receiving and emitting messages)
        self._tasks = []

        # Initialise message queue. Contains messages that need to be delivered to tasks.
        self._messageQueue = queue.Queue()

        # Initialise list of busses. Contains all known busses to this Dispatcher.
        self._busses = []

        # Start the task-thread
        self._run = True
        _thread.start_new_thread(self.run, ())

    def run(self):
        while self._run:
            if self._messageQueue.empty():
                time.sleep(0.1)
            else:
                #send event to all tasks
                try:
                    message = self._messageQueue.get_nowait()
                    for t in self._tasks:
                        try:
                            t.process(message)
                        except:
                            # well, catching all can suck. but we do not want the Dispatcher to crash due to a failing task
                            print("Caught Error while processing message with task")
                            print(repr(sys.exc_info()[0]))
                except queue.Empty:
                    pass

    def emit(self, method, ident, fields = {}):
        """Emits a sOss-Message to all tasks and busses.
        method: Method type (request, response, event, announcement)
        id:     something (unique) that identifies this Message
        fields:  additional fields for this message"""

        fields["method"] = method
        fields["id"] = ident

        # add task to message-queue
        self._messageQueue.put(fields, block=False)

        #send the message to the connected busses
        for b in self._busses:
            b.send(fields)

    def registerTask(self, t):
        """Registers a task to the Dispatcher.
        task:  the task to register"""

        self._tasks.append(t)

    def receiveFromBus(self, bus, message):
        # enqueue this message to be processed by tasks
        self._messageQueue.put(message, block=False)

        # send this message to every bus (but not to the one it received it)
        for b in self._busses:
            if bus is not b:
                b.send(message)

    def registerBus(self, bus):
        """Register a bus to the Dispatcher.
        bus: the bus to register"""

        self._busses.append(bus)

